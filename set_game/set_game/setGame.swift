//
//  setGame.swift
//  set_game
//
//  Created by Арман on 7/21/18.
//  Copyright © 2018 Арман. All rights reserved.
//

import Foundation

class setGame {
    private var deckOfCards = [Card]()
    private(set) var choosenCards = [Card]()
    private(set) var visibleCards = [Card]()
    private(set) var hintCards = [Card]()
    private(set) var score = 0
    
    func startGame() {
        deckOfCards.removeAll()
        visibleCards.removeAll()
        choosenCards.removeAll()
        hintCards.removeAll()
        score = 0
        let range = 1...3
        for number in range {
            for shape in range {
                for color in range {
                    for fill in range {
                        let card = Card(shape, number, color, fill)
                        deckOfCards.append(card)
                    }
                }
            }
        }
        deckOfCards.shuffle()
        for _ in 0...11 {
            visibleCards.append(deckOfCards.removeLast())
        }
    }
    
    func hint() -> Bool {
        for indexFirst in visibleCards.indices {
            for indexSecond in visibleCards.indices {
                for indexThird in visibleCards.indices {
                    if visibleCards[indexFirst] != visibleCards[indexSecond] &&
                        visibleCards[indexSecond] != visibleCards[indexThird] &&
                        visibleCards[indexFirst] != visibleCards[indexThird] {
                        if isThisSet([visibleCards[indexFirst], visibleCards[indexSecond], visibleCards[indexThird]]) &&
                            !(choosenCards.contains(visibleCards[indexFirst]) || choosenCards.contains(visibleCards[indexSecond]) ||
                            choosenCards.contains(visibleCards[indexThird])){
                            hintCards += [visibleCards[indexFirst], visibleCards[indexSecond], visibleCards[indexThird]]
                            score -= 6
                            return true
                        }
                    }
                }
            }
        }
        if visibleCards.count < 24 {
            moreCards()
        }

        return false
    }
    
    func moreCards() {
        if deckOfCards.count >= 3 {
            var arrayOfNewCards = [Card]()
            for _ in 0...2 {
                arrayOfNewCards.append(deckOfCards.removeFirst())
            }
            if choosenCards.count == 3 {
                if isThisSet(choosenCards) {
                    for index in 0...2 {
                        if let indexForNewCard = visibleCards.index(of: choosenCards[index]) {
                            visibleCards[indexForNewCard] = arrayOfNewCards[index]
                        }
                    }
                    choosenCards.removeAll()
                } else if visibleCards.count < 24 {
                    visibleCards += arrayOfNewCards
                }
            } else if visibleCards.count < 24 {
                visibleCards += arrayOfNewCards
            }
        } else {
            if choosenCards.count == 3 &&  isThisSet(choosenCards){
                for index in 0...2 {
                    if let indexForNewCard = visibleCards.index(of: choosenCards[index]) {
                        visibleCards.remove(at: indexForNewCard)
                    }
                }
            }
        }

    }
    
    func chooseCard(for index: Int) {
        let card = visibleCards[index]
        if !choosenCards.contains(visibleCards[index]) && choosenCards.count == 3 {
            if isThisSet(choosenCards) {
                moreCards()
            }
            choosenCards.removeAll()
        }
            switch choosenCards.count {
            case 0:
                choosenCards.append(card)
            case 1:
                if card != choosenCards[0] {
                    choosenCards.append(card)
                } else {
                    choosenCards.remove(at: 0)
                }
            case 2:
                if card != choosenCards[1] && card != choosenCards[0] {
                    choosenCards.append(card)
                    if isThisSet(choosenCards) {                        
                        score += 2
                    } else {
                        score -= 5
                    }
                } else {
                    if card == choosenCards[1] {
                        choosenCards.remove(at: 1)
                    } else if card == choosenCards[0] {
                        choosenCards.remove(at: 0)
                    }
                }
            default:
                print("ooooh, something wrong")
            }
    }
    
    func isThisSet(_ setCards: [Card]) -> Bool {
//        Если каждая характеристика у всех разная или одинаковая, то это сет
        if ((setCards[0].shape == setCards[1].shape && setCards[1].shape == setCards[2].shape) ||
            (setCards[0].shape != setCards[1].shape && setCards[1].shape != setCards[2].shape &&
                setCards[0].shape != setCards[2].shape)) &&
            ((setCards[0].number == setCards[1].number && setCards[1].number == setCards[2].number) ||
            (setCards[0].number != setCards[1].number && setCards[1].number != setCards[2].number &&
                setCards[0].number != setCards[2].number)) &&
            ((setCards[0].color == setCards[1].color && setCards[1].color == setCards[2].color) ||
            (setCards[0].color != setCards[1].color && setCards[1].color != setCards[2].color &&
                setCards[0].color != setCards[2].color)) &&
            ((setCards[0].fill == setCards[1].fill && setCards[1].fill == setCards[2].fill) ||
            (setCards[0].fill != setCards[1].fill && setCards[1].fill != setCards[2].fill &&
                setCards[0].fill != setCards[2].fill)) {
                return true
        } else {
            return false
        }
    }

    
    init() {
        startGame()
    }
}


