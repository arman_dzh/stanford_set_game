//
//  Card.swift
//  set_game
//
//  Created by Арман on 7/21/18.
//  Copyright © 2018 Арман. All rights reserved.
//

import Foundation

struct Card: Hashable {
    
    let shape: Shape
    let number: Number
    let color: Color
    let fill: Fill
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return (lhs.shape == rhs.shape && lhs.number == rhs.number && lhs.color == rhs.color && lhs.fill == rhs.fill)
    }
    
    static func != (lhs: Card, rhs: Card) -> Bool {
        return (lhs.shape != rhs.shape || lhs.number != rhs.number || lhs.color != rhs.color || lhs.fill != rhs.fill)
    }
    
    init(_ shape: Int, _ number: Int, _ color: Int, _ fill: Int) {
        self.shape = Shape(rawValue: shape)!
        self.number = Number(rawValue: number)!
        self.color = Color(rawValue: color)!
        self.fill = Fill(rawValue: fill)!
    }
    
    
    
    enum Shape: Int, CaseIterable {
        case triangle = 1, circle, square
    }
    enum Number: Int, CaseIterable {
        case one = 1, two, three
    }
    enum Color: Int, CaseIterable {
        case purple = 1, blue, yellow
    }
    enum Fill: Int, CaseIterable {
        case outline = 1, filled, striped
    }
    
}


