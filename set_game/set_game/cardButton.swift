//
//  cardButton.swift
//  set_game
//
//  Created by Арман on 7/21/18.
//  Copyright © 2018 Арман. All rights reserved.
//

import UIKit

class cardButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 1.0
        //layer.borderColor = UIColor.white.cgColor
        layer.cornerRadius = 18
        layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
