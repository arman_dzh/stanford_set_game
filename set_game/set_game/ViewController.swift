//
//  ViewController.swift
//  set_game
//
//  Created by Арман on 7/21/18.
//  Copyright © 2018 Арман. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    
    private(set) var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    private var game = setGame()
    
    @IBOutlet var cardButtons: [cardButton]!
    
    private var symbols = ["▲", "●", "■"]
    
    private var colors = [#colorLiteral(red: 1, green: 0.2605737448, blue: 1, alpha: 1), #colorLiteral(red: 0.0119254645, green: 0.2061552703, blue: 1, alpha: 1), #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)]
    
    private var fills = ["outline", "filled", "stripped"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewFromModel()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction private func touchCard(_ sender: cardButton) {
        if let cardIndex = cardButtons.index(of: sender) {
            //print(cardIndex)
            game.chooseCard(for: cardIndex)
            updateViewFromModel()
        } else {
            print("card not in the array")
        }
    }
    
    
    @IBAction private func hint(_ sender: UIButton) {
        if game.hint(){
            updateViewFromModel()
        } else {
            print("There is no hint")
            updateViewFromModel()
        }
    }
    @IBAction private func moreCards(_ sender: UIButton) {
        if game.visibleCards.count < 24 || game.choosenCards.count == 3 {
            game.moreCards()
        }
        updateViewFromModel()
    }
    
    @IBAction private func newGame(_ sender: UIButton) {
        game.startGame()
        updateViewFromModel()
    }
    
    private func updateViewFromModel(){
        score = game.score
        for index in cardButtons.indices {
            if game.visibleCards.indices.contains(index) {
                cardButtons[index].setAttributedTitle(makeCardAppearance(for: game.visibleCards[index]), for: .normal)
                cardButtons[index].backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cardButtons[index].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cardButtons[index].isEnabled = true
                if game.hintCards.contains(game.visibleCards[index]) {
                    cardButtons[index].layer.borderColor = #colorLiteral(red: 0.1327495277, green: 0.1333712935, blue: 0.1348648667, alpha: 1)
                }
                
                if game.choosenCards.contains(game.visibleCards[index]) && game.choosenCards.count == 3 {
                    if game.isThisSet(game.choosenCards){
                        cardButtons[index].backgroundColor = #colorLiteral(red: 0.05815209448, green: 0.05781445652, blue: 0.05841680616, alpha: 1)
                        cardButtons[index].layer.borderColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                    } else {
                        cardButtons[index].backgroundColor = #colorLiteral(red: 0.05815209448, green: 0.05781445652, blue: 0.05841680616, alpha: 1)
                        cardButtons[index].layer.borderColor = #colorLiteral(red: 0.9838864207, green: 0.1569570303, blue: 0.02957256883, alpha: 1)
                    }
                } else if game.choosenCards.contains(game.visibleCards[index]) {
                    cardButtons[index].backgroundColor = #colorLiteral(red: 0.05815209448, green: 0.05781445652, blue: 0.05841680616, alpha: 1)
                    cardButtons[index].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                
            } else {
                cardButtons[index].setAttributedTitle(nil, for: .normal)
                cardButtons[index].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
                cardButtons[index].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cardButtons[index].isEnabled = false
            }
        }
    }
    
    private func makeCardAppearance(for card: Card) -> NSAttributedString {
        let symbol = getShape(for: card)
        var shapes = ""
        let fontSize = CGFloat(21)
        let times = getNumberOfShapes(for: card)
        for index in 1...times {
            if index > 1 {
                shapes.append(" ")
            }
            shapes.append(symbol)
        }
        //shapes.separate(with: " ")
        let fill = getFill(for: card)
        let attributedShapes: [NSAttributedString.Key : Any]
        switch fill {
        case "outline":
            attributedShapes = [NSAttributedString.Key.strokeWidth: 8.0,
                                NSAttributedString.Key.foregroundColor: getColor(for: card, with: 1),
                                NSAttributedString.Key.strokeColor: getColor(for: card, with: 1),
                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)]
        case "stripped":
            
            attributedShapes = [NSAttributedString.Key.strokeWidth: -8.0,
                                NSAttributedString.Key.foregroundColor: getColor(for: card, with: 0.5),
                                NSAttributedString.Key.strokeColor: getColor(for: card, with: 1),
                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)]
        case "filled":
            attributedShapes = [NSAttributedString.Key.strokeWidth: -8.0,
                                NSAttributedString.Key.foregroundColor: getColor(for: card, with: 1),
                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)]
        default:
            print("something went wrong")
            attributedShapes = [NSAttributedString.Key.strokeWidth: 0,
                                NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1) ]
        }
        //print(NSAttributedString(string: shapes, attributes: attributedShapes))
        return NSAttributedString(string: shapes, attributes: attributedShapes)
    }
    
    private func getShape(for card: Card) -> String {
        return symbols[card.shape.rawValue - 1]
    }
    
    private func getNumberOfShapes(for card: Card) -> Int {
        return card.number.rawValue
    }
    
    private func getColor(for card: Card, with alpha: CGFloat) -> UIColor {
        return colors[card.color.rawValue - 1].withAlphaComponent(alpha)
    }
    
    private func getFill(for card: Card) -> String {
        return fills[card.fill.rawValue - 1]
    }
}


extension String {
    mutating func separate(with string: String) {
        var new = " "
        var count = 0
        for char in self {
            count += 1
            new += String(char)
            if count != count{
                new += string
            }

        }
        self = new
    }
}
